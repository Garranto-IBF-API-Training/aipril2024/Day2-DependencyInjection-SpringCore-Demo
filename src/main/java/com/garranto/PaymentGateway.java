package com.garranto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component(value = "gateway")
public class PaymentGateway {


//    private PaypalPaymentProcessor paymentProcessor = new PaypalPaymentProcessor("");
    @Autowired
    @Qualifier("paypal")
    private PaymentProcesser paymentProcessor;

    public PaymentGateway(PaymentProcesser paymentProcessor){
        System.out.println("gateway created using single argument constructor");
        this.paymentProcessor = paymentProcessor;
    }

    public PaymentGateway(String d1,String d2){
        System.out.println("Gateway created using 2 argument constructor");
    }

    public PaymentGateway(){
        System.out.println("Gateway created using no argument constructor");
    }

    public PaymentProcesser getPaymentProcessor() {
        return paymentProcessor;
    }

    public void setPaymentProcessor(PaymentProcesser paymentProcessor) {
        this.paymentProcessor = paymentProcessor;
    }

    public void processPayment(double amount){
        System.out.println("Performing Security check");
        paymentProcessor.payment(amount);

    }

    public void getTransactionStatus(String id){
        System.out.println("Performing Security check");
        String status = paymentProcessor.findTransactionStatus(id);
        System.out.println("The transaction is: "+status);
    }
}


//if the application components are tightly coupled -- they are not flexible --- they will be rigid --- it will be fragile

//if the components are loosely coupled, the application wil be flexible --- the application will be stable -- components can be managed independently

//processor  gateway client

//inject the dependency rather than making the dependent directly managing its dependency



//understanding di and ioc


//@configuration --  set the configuration class
//@ComponentScan -- set the location of the component classes
//@Componennt -- set the component class
//@Autowired -- to automaticall inject the dependent beans
//@Qualifier -- Autoweiring based on name
//@Primary -- set primary candidate for autowiring based on type


//Spring boot
//spring makes working with java easier
//spring boot makes it easy to work with spring
