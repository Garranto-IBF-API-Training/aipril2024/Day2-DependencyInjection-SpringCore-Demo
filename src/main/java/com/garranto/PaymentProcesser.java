package com.garranto;

public interface PaymentProcesser {

    //    provide specification for what is to be implemented by the payment processors
    void payment(double amount);

    String findTransactionStatus(String tranID);
}
