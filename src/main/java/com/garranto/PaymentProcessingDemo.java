package com.garranto;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Scanner;

public class PaymentProcessingDemo {
    public static void main(String[] args) {
        System.out.println("Welcome to my payment processing App");
        System.out.println("=======================================");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the amount to process");
        double amount = scanner.nextDouble();
        System.out.println("Enter the trnsaction id");
        String id = scanner.next();

//        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        ApplicationContext context = new AnnotationConfigApplicationContext(PaymentProcessingBeanConfigurations.class);

        PaymentGateway gateway = context.getBean("gateway",PaymentGateway.class);
//        PaypalPaymentProcessor paypal = context.getBean("paypalOne",PaypalPaymentProcessor.class);
        System.out.println("initial payment processor"+gateway.getPaymentProcessor());
//        gateway.setPaymentProcessor(paypal);
        System.out.println("New Payment processor"+gateway.getPaymentProcessor());
        gateway.processPayment(amount);
        gateway.getTransactionStatus(id);


    }
}


//PaymentProcessor
//PaymentGateway

//through di we have


//public static void main(String[] args) {
//        System.out.println("Welcome to my payment processing App");
//        System.out.println("=======================================");
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Enter the amount to process");
//        double amount = scanner.nextDouble();
//        System.out.println("Enter the trnsaction id");
//        String id = scanner.next();
//
////        PaymentProcessingFactory factory = new PaymentProcessingFactory();
//        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
//
//
//
////        PaypalPaymentProcessor paypalPaymentProcessor = factory.getPayPal();
//        PaypalPaymentProcessor paymentProcessor = context.getBean("paypalOne",PaypalPaymentProcessor.class);
//        StripePaymentProcessor stripePaymentProcessor = context.getBean("stripeOne",StripePaymentProcessor.class);
////        PaymentGateway gateway = factory.getGateway(paypalPaymentProcessor);
//        PaymentGateway gateway = context.getBean("gateway",PaymentGateway.class);
//        gateway.processPayment(amount);
//        gateway.getTransactionStatus(id);
//
//
//    }
