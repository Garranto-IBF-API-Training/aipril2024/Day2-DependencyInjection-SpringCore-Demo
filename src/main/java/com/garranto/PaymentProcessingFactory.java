package com.garranto;

public class PaymentProcessingFactory {

    public PaypalPaymentProcessor getPayPal(){
        return new PaypalPaymentProcessor();
    }

    public StripePaymentProcessor getStripe(){
        return new StripePaymentProcessor();
    }

    public PaymentGateway getGateway(PaymentProcesser paymentProcesser){
        return new PaymentGateway(paymentProcesser);
    }
}

//create and manage the components required in the application
