package com.garranto;

import org.springframework.stereotype.Component;

@Component(value = "paypal")
public class PaypalPaymentProcessor implements PaymentProcesser {


//    public PaypalPaymentProcessor(String desc){}

    public PaypalPaymentProcessor(){
        System.out.println("paypal payment processor is created");
    }
    public void payment(double amount){
        System.out.println("Payment processed by paypal");
        System.out.println("Payment of amount"+amount+"is success");
    }

    public String findTransactionStatus(String tranID){
        return "PENDING";
    }
}
