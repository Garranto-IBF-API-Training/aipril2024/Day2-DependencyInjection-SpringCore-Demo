package com.garranto;

import org.springframework.stereotype.Component;

@Component(value = "stripe")
public class StripePaymentProcessor implements PaymentProcesser {

    public StripePaymentProcessor(){
        System.out.println("Stripe payment processor is created");
    }
    public void payment(double amount){
        System.out.println("Payment processed by Stripe");
        System.out.println("Payment of amount"+amount+"is success");
    }

    public String findTransactionStatus(String tranID){
        return "PENDING";
    }
}
