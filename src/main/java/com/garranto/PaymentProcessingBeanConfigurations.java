package com.garranto;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan(basePackages = "com.garranto")
public class PaymentProcessingBeanConfigurations {


//    @Bean(value = "gateway")
//    public PaymentGateway gateway(PaymentProcesser stripe){
////        PaymentGateway paymentGateway = new PaymentGateway();
////        paymentGateway.setPaymentProcessor(paypal);
//        return new PaymentGateway(stripe);
//    }
//
//    @Bean(value = "paypal")
//    @Primary
//    public PaypalPaymentProcessor paypalOne(){
//        return new PaypalPaymentProcessor();
//    }
//
//    @Bean(value = "stripe")
//    @Lazy
//    public StripePaymentProcessor stripeOne(){
//        return new StripePaymentProcessor();
//    }
}


// <bean id="gateway" class="com.garranto.PaymentGateway" lazy-init="true">
//        <property name="paymentProcessor" ref="paypalOne" />
//    </bean>

//    @Bean(value = "gateway")
//    public PaymentGateway gateway(){
//        PaymentGateway paymentGateway = new PaymentGateway();
//        paymentGateway.setPaymentProcessor(new PaypalPaymentProcessor());
//        return new PaymentGateway();
//    }
